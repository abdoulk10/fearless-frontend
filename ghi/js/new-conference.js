window.addEventListener('DOMContentLoaded', async () => {
    const formTag = document.getElementById('create-conference-form');
    const locationSelect = document.getElementById('location');


    const locationsResponse = await fetch('http://localhost:8000/api/locations/');
    const locationsData = await locationsResponse.json();

    locationsData.locations.forEach(location => {
        const option = document.createElement('option');
        option.value = location.id; // Use the "id" property as the option value
        option.textContent = location.name;
        locationSelect.appendChild(option);
    });


    formTag.addEventListener('submit', async event => {
        event.preventDefault();

        try {
            const formData = new FormData(formTag);
            const conferenceData = {
                name: formData.get('name'),
                starts: formData.get('starts'),
                ends: formData.get('ends'),
                description: formData.get('description'),
                max_presentations: parseInt(formData.get('max_presentations')),
                max_attendees: parseInt(formData.get('max_attendees')),
                location: parseInt(formData.get('location')),
            };
            const json = JSON.stringify(conferenceData);

            const url = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
                method: 'POST',
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(url, fetchConfig);

            if (response.ok) {
                formTag.reset();
                console.log('Conference created successfully');
            }
            else {
                console.error('Conference creation failed:', response.statusText);
            }
        } catch (error) {
            console.error('An error occurred:', error);
        }
    });
});
